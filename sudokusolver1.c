#include <stdio.h>

  /* O program tá sem erro de compilação mas ainda tá errado, quando jogo a matriz que ele tem que resolver
  não acontece nada, ele nem a lê
5 3 X X 7 X X X X
6 X X 1 9 5 X X X
X 9 8 X X X X 6 X
8 X X X 6 X X X 3
4 X X 8 X 3 X X 1
7 X X X 2 X X X 6
X 6 X X X X 2 8 X
X X X 4 1 9 X X 5
X X X X 8 X X 7 9*/

  int check (int line, int column, int n){

    int l, c, region_line, region_column, matriz[9][9];
    char ascii = 'X';

    if(matriz[line][column] == n){
      return 1;
    }
    if(matriz[line][column] != 'X'){
      return 0;
    }
    for(l=0; l<9; l++){
      if(matriz[l][column] == n){
        return 0;
      }
    }
    for(c=0; c<9; c++){
      if(matriz[line][c] == n){
        return 0;
      }
    }

 /*region_line e region_column são submatrizes 3x3 dentro do sudoku 9x9.
 3 region_line e 3 region_column*/

    region_line = l/3;
    region_column = c/3;

    for(l = region_line*3; l < (region_line + 1)*3; l++){
      for(c = region_column*3; c < (region_column + 1)*3; c++){
        if(matriz[l][c] == n){
          return 0;
        }
      }
    }

    return 1;
  }

  int main () {

    int l, c, line, column, n;
    char m[9][9];

    for(l=0; l<9; l++){
      for(c=0; c<9; c++){
        scanf(" %c", &m[l][c]);
      }
    }

    //check(line, column, n);

    if(l == 9){
      for(l=0; l<9; l++){
        for(c=0; c<9; c++){
          printf("%d ", m[l][c]);
          if(l % 3 == 2){
            printf("-");
          }
          if(c % 3 == 2){
            printf("|");
          }
      } printf("\n" );
      }
    }

    return 0;
  }
