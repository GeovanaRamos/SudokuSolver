#include "stdio.h"

int i,j, teste;
int m[9][9];


void mostrar(int m[9][9]){
    for(i=0; i<9; i++){
        for(j=0; j<9; j++){
            printf("%d", m[i][j]);

            if (j == 8){
                printf("\n");
            } else if (j == 2 || j == 5){
                printf(" | ");
            } else {
                printf(" ");
            }
        }

        if(i == 2 || i == 5){
            printf("- - - - - - - - - - -\n");
        }
    }
}
int verificar_linha(int i, int j, int teste){
    for (j=0; j<9; j++){
            if (teste == m[i][j])
                return 0;
    }
    return 1;
}
int verificar_coluna(int i, int j, int teste){
    for (i=0; i<9; i++){
            if (teste == m[i][j])
                return 0;
    }
    return 1;
}
int verificar_bloco(int i, int j, int teste){
    int p, q;
    for (p=(i/3)*3; p<((i/3)+1)*3; p++){
        for (q=(j/3)*3; q<((j/3)+1)*3; q++){
            if (m[p][q] == teste)
                return 0;
        }
    }
    return 1;
}

int teste_Total(int i, int j, int teste ){
    if (verificar_bloco(i,j,teste)==1 && verificar_coluna(i, j, teste) == 1 && verificar_linha(i, j,teste) == 1)
        return teste;
    else
        return 0;
}

int teste_valores(int i, int j){
    int possivel = 0, resposta=0;

    for(teste=1; teste<=9;teste++){
        m[i][j] = teste_Total(i,j,teste);
        if (m[i][j] != 0){
            possivel++;
            resposta = teste;
        }

    }
    if (possivel!=1)
        return 40;
    else
        return resposta;
}

void resolver(int m[9][9]){
    for (i=0;i<9;i++){
        for(j=0;j<9;j++){
            if(m[i][j] == 40){
                m[i][j] = teste_valores(i,j);
            }
        }
    }

}


int main(){
    int t=1;
    char m1[i][j];

    for (i=0;i<9;i++){
        for(j=0;j<9;j++){
            scanf(" %c", &m1[i][j] );
            m[i][j] = m1[i][j] - 48;
        }
    }
    resolver(m);

    while (t!=0) {
        t=0;
        for (i=0;i<9;i++){
            for(j=0;j<9;j++){
                if (m[i][j] == 40)
                    t++;
            }
        }
        if(t>0)
        resolver(m);
    }

    mostrar(m);

    return 0;

}
